from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        exclude = ("purchaser",)
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        exclude = ("owner",)
        fields = ("name",)


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        exclude = ("owner",)
        fields = ("name", "number",)
